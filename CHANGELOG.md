# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [1.1.1] - 2023-02-25
### Added
- dist folder

## [1.1.0] - 2023-02-21
### Fixed
- Text erase when update style selected part #1

## [1.0.0] - 2021-01-11
### Added
- Initial import

[Unreleased]: https://gitlab.com/hydrana/summernote-pages/compare/v1.1.1...master
[1.1.1]: https://gitlab.com/hydrana/summernote-pages/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/hydrana/summernote-pages/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/hydrana/summernote-pages/tags/v1.0.0
